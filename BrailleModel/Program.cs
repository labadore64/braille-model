﻿using System;
using BrailleModel.model.braille;

namespace BrailleModel
{
    class MainClass
    {
        public static void Main(string[] args)
        {
            BrailleCharacters brailleCharacters = new BrailleCharacters("resources/grade1_en.txt");
            string[] chars = { "a", "b", "c", "d", "e", "f", "g", "h", "i", "j", "k", "l", "m", "n", "o", "p", "q", "r", "s", "t", "u", "v", "w", "x", "y", "z" };

            for(int i = 0; i < chars.Length; i++)
            {
                brailleCharacters.PrintDefinition(chars[i]);
            }
        }
    }
}
