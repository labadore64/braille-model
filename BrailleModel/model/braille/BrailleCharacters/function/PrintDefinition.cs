﻿using System;

namespace BrailleModel.model.braille
{
    public partial class BrailleCharacters
    {
        public void PrintDefinition(string ID)
        {
            if (Characters.ContainsKey(ID))
            {
                Console.WriteLine(Characters[ID]);
            }
            else
            {
                Console.WriteLine("that key does not exist...");
            }

            Console.WriteLine(" ");
        }
    }
}