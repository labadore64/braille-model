﻿using System.Collections.Generic;

namespace BrailleModel.model.braille
{
    public partial class BrailleCharacters
    {
        public Dictionary<string, BrailleCell> Characters { get; private set; } = new Dictionary<string, BrailleCell>();
    }
}