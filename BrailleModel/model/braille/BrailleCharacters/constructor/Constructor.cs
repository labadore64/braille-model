﻿using System;

namespace BrailleModel.model.braille
{
    public partial class BrailleCharacters
    {

        public BrailleCharacters(string FileName)
        {
            try
            {
                // gets all the strings of the source braille file
                string[] characters = System.IO.File.ReadAllLines(FileName);

                int RowCount = Int32.Parse(characters[0][characters[0].Length-1].ToString());
                int ColumnCount = Int32.Parse(characters[1][characters[1].Length - 1].ToString());



                for (int i = 2; i < characters.Length; i += RowCount+1){

                    // gets the character represented, which should
                    // always be the first character.
                    string character = characters[i];

                    // get the cells
                    bool[][] cells = new bool[RowCount][];

                    // holds columns
                    bool[] row;

                    // array for split strings
                    string[] split;

                    // populates cells
                    for(int j = 0; j < RowCount; j++)
                    {
                        row = new bool[ColumnCount];
                        split = characters[i + j + 1].Split(Delimiter);

                        // populate row
                        for(int k = 0; k < ColumnCount; k++)
                        {
                            if(k >= split.Length)
                            {
                                row[k] = false;
                            }
                            else
                            {
                                if(split[k] == Empty ||
                                    split[k].Length == 0)
                                {
                                    row[k] = false;
                                }
                                else
                                {
                                    row[k] = true;
                                }
                            }
                        }

                        cells[j] = row;
                    }

                    // adds new character

                    Characters.Add(character,
                                    new BrailleCell(
                                            character,
                                            cells
                                        )
                                    );
                }
            } catch
            {
                throw;
            }
        }
    }
}