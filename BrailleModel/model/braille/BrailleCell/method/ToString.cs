﻿
using System.Text;

namespace BrailleModel.model.braille
{
    public partial struct BrailleCell
    {
        public override string ToString()
        {
            StringBuilder stringer = new StringBuilder();

            stringer.Append("Character: " + ID);

            for(int i = 0; i < Cells.Length; i++)
            {
                stringer.Append("\n");
                for (int j = 0; j < Cells[i].Length; j++)
                {
                    if (Cells[i][j])
                    {
                        stringer.Append("● ");
                    }
                    else
                    {
                        stringer.Append("  ");
                    }
                }
            }

            return stringer.ToString();
        }
    }
}
