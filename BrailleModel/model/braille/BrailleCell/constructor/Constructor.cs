﻿
namespace BrailleModel.model.braille
{
    public partial struct BrailleCell
    {
        public BrailleCell(string ID, bool[][] Cells)
        {
            this.ID = ID;
            this.Cells = Cells;
        }
    }
}
